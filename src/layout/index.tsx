import React from 'react';
import {Avatar, Divider, Image, Layout, Space, Typography} from "antd"
import {UserOutlined} from '@ant-design/icons'
import {renderRoutes} from "react-router-config";
import {routes} from "../routes";
import {Link} from "react-router-dom";
import './index.css'
import {selector, atom, useRecoilValue} from 'recoil';
import axios from "axios";
import {v4} from "uuid";

const UserListStateRefresh = atom({
    key: 'UserListStateRefresh',
    default: ''
});

export const UserListState = selector({
    key: 'UserListState',
    set: ({set}) => set(UserListStateRefresh, v4()),
    get: async ({get}) => {
        get(UserListStateRefresh);
        const {data} = await axios.post('/api/user/all');
        return data
    }
});

export const UserState = selector({
    key: 'UserState',
    get: async ({get}) => {
        const userList = get(UserListState);
        const userId = localStorage.getItem('userId');
        return userList.find((user: any) => user.id === userId)
    }
});

export const LayoutComponent: React.FC = () => {
    const user = useRecoilValue(UserState);
    return (
        <Layout className="layout" style={{height: '100%'}}>
            <Layout.Header style={{background: '#fff', display: "flex", justifyContent: 'space-between'}}>
                <div style={{padding: 15}}>
                    <Image
                        width={110}
                        height={54}
                        src="error"
                        fallback="https://static.tildacdn.com/tild6230-3162-4135-a466-356133373266/Balance_Platform_Log.svg"
                    />
                </div>
                <div className={'MenuLink'}>
                    <Space split={<Divider type="vertical"/>}>
                        <Link to="/cookieclicker">КукиКликер</Link>
                        <Link to="/speedtest">Скорость печати</Link>
                        <Link to="/formtest">Весёлый Tab</Link>
                        <Link to="/searchtest">Внимание док</Link>
                        <Link to="/lk">Личный кабинет</Link>
                        <Link to="/verification">Верификация</Link>
                    </Space>
                </div>
                <div>
                    <Avatar style={{marginRight: 5}} size={32} icon={<UserOutlined/>}/>
                    <Typography.Text>{user && user.name}</Typography.Text>
                </div>
            </Layout.Header>
            <Layout.Content style={{background: '#fff'}}>
                {renderRoutes(routes)}
            </Layout.Content>
        </Layout>
    )
}