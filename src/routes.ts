import CookieClicker from "./cookieClicker";
import FormTest from "./formTest";

import SpeedTest from "./speedTest";
import {LkComponent} from "./lk";
import {VerificationComponent} from "./verification";
import SearchTest from "./searchTest";

export const routes = [
    {
        path: "/cookieclicker",
        exact: true,
        component: CookieClicker,
    },
    {
        path: "/speedtest",
        component: SpeedTest,
    },
    {
        path: "/formtest",
        component: FormTest,
    },
    {
        path: "/lk",
        component: LkComponent,
    },
    {
        path: "/verification",
        component: VerificationComponent,
    },
    {
        path: "/searchtest",
        component: SearchTest,
    }
];
