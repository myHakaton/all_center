import React, { useState, useEffect, useRef } from "react";
import { GameButton, ItemList } from "../components";
import { useGame, useSendGameData } from "../hooks";
import { random } from "../utils/randomArray";
import { docList } from "../utils/docList";

import styles from "./cookie.module.css";
import { SearchGame } from "../types";
import { countAccuracy } from "../utils/accuracy";

export function SearchTest() {
  const [clicks, setClicks] = useState(0);
  const [pressedKey, setPressedKey] = useState();
  const [fails, setFails] = useState(0);
  const getData = () => ({
    points: clicks,
    gameInfo: {
      errors: fails,
      accuracy: countAccuracy(clicks, fails),
      timer: 0,
    },
  });
  const { handleStart, gameState, time } = useGame<SearchGame>(
    "searchGame",
    getData
  );

  const sendData = useSendGameData("tabGame");
  useEffect(() => {
    if (gameState === "end")
      sendData(clicks, {
        errors: fails,
        accuracy: countAccuracy(clicks, fails),
        timer: 0,
      });
  }, [gameState]);

  const doc = docList.find((x) => x.type === "passportRF")!; // random(docList);
  const [activeZone, setActiveZone] = useState<any>(random(doc.values));
  const changeActiveZone = () => {
    setActiveZone(random(doc.values.filter((x) => x.name !== activeZone.name)));
  };
  // useEffect(() => {
  //   let timer = setTimeout(function timeout() {
  //     if (
  //       inputRefs.current[activeElementIndex.current] !==
  //         document.activeElement &&
  //       gameState === "start"
  //     )
  //       setTimeWithoutFocus((t) => t + 1);
  //     timer = setTimeout(timeout, 100);
  //     if (gameState === "end") clearTimeout(timer);
  //   }, 100);
  // }, [gameState]);

  const startGame = () => {
    handleStart();
  };

  useEffect(() => {
    document.body.addEventListener("click", (e) => {
      e.preventDefault();
    });
  }, []);

  useEffect(() => {
    if (pressedKey) {
      const timer = setTimeout(() => setPressedKey(undefined), 700);
      return () => clearTimeout(timer);
    }
  }, [pressedKey]);
  return (
    <div className="container-fluid pt-4">
      <div className="row">
        <div className="col-sm-6 col-md-2 order-md-2 px-5">
          <ul className="list-unstyled text-center small">
            <ItemList name="Время" data={time} />
            <ItemList name="Ошибки" data={fails} />
            <ItemList name="Аккуратность" data={countAccuracy(clicks, fails)} />
          </ul>
        </div>
        {/* Left */}
        <div className="col-sm-6 col-md-2 order-md-0 px-5">
          <ul className="list-unstyled text-center small">
            <ItemList name="Очки" data={clicks} />
          </ul>
        </div>
        {/* Body */}
        <div className="col-sm-12 col-md-8 order-md-1">
          <div className="container">
            <div className="text-center mt-4 header">
              <div className="alert alert-danger" role="alert">
                Сопоставьте название поля с его значением на картинке. Не
                пытайтесь нажимать <b>куда-попало</b>. За это вам будут
                засчитаны штрафные баллы.
              </div>

              <div className="control my-5">
                <span className="btn-circle-animation" />
              </div>
              <div className="control my-5">
                <GameButton state={gameState} handleStart={startGame} />
                <span className="btn-circle-animation" />
              </div>
            </div>
            <div style={{ display: "flex" }}>
              <div
                style={{ minWidth: 500 }}
                onClick={() => setFails((f) => f + 1)}
              >
                <div
                  onClick={(e) => {
                    e.stopPropagation();
                    setClicks((c) => c + 1);
                    changeActiveZone();
                  }}
                  style={{
                    position: "absolute",
                    // width: 20,
                    // height: 20,
                    // marginTop: 200,
                    // backgroundColor: 'red'
                  }}
                  className={styles[`${doc.type}_${activeZone.name}`]}
                ></div>
                <img style={{ width: "100%" }} src={doc.img} alt={doc.type} />
              </div>

              <div>
                {gameState === "start" && (
                  <span className="d-block display-4">
                    Найдите: {activeZone.label}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
