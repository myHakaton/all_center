import {Button, Col, Form, Row, Select} from "antd";
import axios from "axios";
import {useEffect, useState} from "react";

export const SelectedUserComponent = () => {
    const [users, setUsers] = useState<any[]>([]);
    useEffect(() => {
        axios.post('/api/user/all').then(({data}) => setUsers(data));
    }, []);
    return (
        <Row justify={"center"}>
            <Col span={8} style={{padding: 100}}>
                <Form layout={'vertical'} onFinish={({userId}) => {
                    localStorage.setItem('userId', userId);
                    window.location.reload();
                }}>
                    <Form.Item label={'Оператор'} name={'userId'} rules={[{required: true}]}>
                        <Select options={users.map(item => ({
                            label: item.name,
                            value: item.id
                        }))}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type={"primary"} htmlType={'submit'}>
                            Выбрать оператора
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    )
}