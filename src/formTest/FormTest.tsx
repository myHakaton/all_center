import { Typography } from "antd";
import React, { useState, useEffect, useRef } from "react";
import { GameButton, ItemList } from "../components";
import { useGame, useSendGameData } from "../hooks";
import { TabGame } from "../types";
import { random } from "../utils/randomArray";

import styles from "./cookie.module.css";

export function FormTest() {
  const [clicks, setClicks] = useState(0);
  const [timeWithoutFocus, setTimeWithoutFocus] = useState(0);
  const [pressedKey, setPressedKey] = useState();
  const [fails, setFails] = useState(0);
  const getData = () => ({
    points: clicks,
    gameInfo: {
      accuracy: 0,
      timer: 0,
    }
  })
  const { handleStart, gameState, time } = useGame<TabGame>("tabGame", getData);
  const sendData = useSendGameData("tabGame");
  useEffect(() => {
    if (gameState === "end")
      sendData(clicks, { errors: fails, timer: 0 })
  }, [gameState])
  const activeElementIndex = useRef(0);
  const inputRefs = useRef<HTMLInputElement[]>([]);
  inputRefs.current = [];

  const checkFail = ({
    delay = 10,
    time = 150,
  }: {
    delay: number;
    time: number;
  }) =>
    new Promise((resolve, reject) => {
      let ticks = 0;
      let timeout = setTimeout(function request() {
        if (
          inputRefs.current[activeElementIndex.current] ===
          document.activeElement
        )
          ticks++;
        else {
          ticks = 0;
          setFails((fails) => fails + 1);
        }
        if (ticks === time) {
          clearTimeout(timeout);
          resolve("stop");
        } else {
          timeout = setTimeout(request, delay);
        }
      }, delay);
    });

  useEffect(() => {
    let timer = setTimeout(function timeout() {
      if (
        inputRefs.current[activeElementIndex.current] !==
          document.activeElement &&
        gameState === "start"
      )
        setTimeWithoutFocus((t) => t + 1);
      timer = setTimeout(timeout, 100);
      if (gameState === "end") clearTimeout(timer);
    }, 100);
  }, [gameState]);

  const addToRefs = (el: HTMLInputElement) => {
    if (el && !inputRefs.current.includes(el)) {
      inputRefs.current.push(el);
      el.onfocus = async (e: any) => {
        const target = inputRefs.current[activeElementIndex.current];
        if (target === document.activeElement) {
          // setTimeout(async () => {
          // e.target.style.animation = "none";
          await checkFail({ delay: 10, time: 150 });
          // target.className = "";
          const refs = inputRefs.current.filter((i) => i !== e.target);
          const newTarget = random(refs);
          activeElementIndex.current = inputRefs.current.indexOf(newTarget);
          inputRefs.current.forEach((i) => (i.className = ""));
          newTarget.className = styles.animatePulse2;
          setClicks((clicks) => clicks + 1);
        }
        // }, 1500);
      };
    }
  };

  const startGame = () => {
    handleStart();
    inputRefs.current[inputRefs.current.length - 1].focus();
  };

  const handleKeyDown = (e: any) => {
    const { key } = e;
    console.log(inputRefs);
    const isTab = key === "Tab";
    if (isTab && e.shiftKey) {
      if (document.activeElement === inputRefs.current[0]) {
        e.preventDefault();
        inputRefs.current[inputRefs.current.length - 1].focus();
      }
    } else if (isTab) {
      if (
        document.activeElement ===
        inputRefs.current[inputRefs.current.length - 1]
      ) {
        console.log(inputRefs.current[0]);
        e.preventDefault();
        inputRefs.current[0].focus();
      }
      // e.preventDefault();
    }
  };

  useEffect(() => {
    document.body.addEventListener("click", (e) => {
      e.preventDefault();
    });
  }, []);

  const handleKeyDown2 = (e: any) => {
    const { key } = e;
    console.log(inputRefs);
    setClicks((clicks) => {
      const isCorrect = key === "Tab";
      if (isCorrect) {
        if (
          document.activeElement ===
          inputRefs.current[inputRefs.current.length - 1]
        )
          inputRefs.current[0].focus();
        e.preventDefault();

        return clicks + 1;
      }
      return clicks;
    });
  };

  useEffect(() => {
    if (gameState === "start") {
      document.addEventListener("keydown", handleKeyDown);
      return () => document.removeEventListener("keydown", handleKeyDown);
    }
  }, [fails, gameState]);

  useEffect(() => {
    if (pressedKey) {
      const timer = setTimeout(() => setPressedKey(undefined), 700);
      return () => clearTimeout(timer);
    }
  }, [pressedKey]);
  return (
    <div className="container-fluid pt-4">
      <div className="row">
        <div className="col-sm-6 col-md-2 order-md-2 px-5">
          <ul className="list-unstyled text-center small">
            <ItemList name="Время" data={time} />
            <ItemList name="Ошибки" data={fails} />
            <ItemList name="Аккуратность" data={1} />
          </ul>
        </div>
        {/* Left */}
        <div className="col-sm-6 col-md-2 order-md-0 px-5">
          <ul className="list-unstyled text-center small">
            <ItemList name="Очки" data={clicks} />
            <ItemList name="Время вне фокуса" data={timeWithoutFocus} />
          </ul>
        </div>
        <div className="col-sm-12 col-md-8 order-md-1">
          <div className="container">
            <div className="text-center mt-4 header">
              <div className="alert alert-danger" role="alert">
                Перед вами тренировка горячих клавиш. Не используйте <b>мышь</b>{" "}
                чтобы менять поле, для это используйте клавиши,{" "}
                <Typography.Text code>Tab</Typography.Text> для переноса курсора
                вниз, <Typography.Text code>Shift</Typography.Text> +{" "}
                <Typography.Text code>Tab</Typography.Text> для переноса курсора
                вверх.
              </div>
              <div className="control my-5">
                <span className="btn-circle-animation" />
              </div>
              <div className="control my-5">
                <GameButton state={gameState} handleStart={startGame} />
                <span className="btn-circle-animation" />
              </div>
            </div>
            <div onClick={(e) => setFails((f) => f + 1000)}>
              <div
                className="control my-5"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <input
                  className={styles.animatePulse2}
                  ref={addToRefs}
                  onClick={(e) => setFails((f) => f + 1000)}
                  tabIndex={1}
                  value={""}
                  // autoFocus
                />
                <span className="btn-circle-animation" />
              </div>
              <div
                className="control my-5"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <input
                  onClick={(e) => setFails((f) => f + 1000)}
                  tabIndex={1}
                  value={""}
                  // autoFocus
                />
                <span className="btn-circle-animation" />
              </div>
              <div
                className="control my-5"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <input
                  ref={addToRefs}
                  value={""}
                  onClick={(e) => setFails((f) => f + 1000)}
                  tabIndex={1}
                  // autoFocus
                />
                <span className="btn-circle-animation" />
              </div>
              <div
                className="control my-5"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <input
                  ref={addToRefs}
                  value={""}
                  onClick={(e) => setFails((f) => f + 1000)}
                  tabIndex={1}
                  // autoFocus
                />
                <span className="btn-circle-animation" />
              </div>
              <div
                className="control my-5"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <input
                  value={""}
                  ref={addToRefs}
                  onClick={(e) => setFails((f) => f + 1000)}
                  tabIndex={1}
                  // autoFocus
                />
                <span className="btn-circle-animation" />
              </div>
              <div
                className="control my-5"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <input
                  ref={addToRefs}
                  value={""}
                  onClick={(e) => setFails((f) => f + 1000)}
                  tabIndex={1}
                  // autoFocus
                />
                <span className="btn-circle-animation" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
