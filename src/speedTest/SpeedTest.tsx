import React, { useState, useRef, useEffect } from "react";
import { quotesArray, random, allowedKeys } from "./Helper";
import ItemList from "./ItemList";
import "./App.css";
import { GameButton } from "../components";
import { useGame, useSendGameData } from "../hooks";
import { GameState, SpeedGame } from "../types";

let interval: any = null;

export const SpeedTest = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const outputRef = useRef<HTMLInputElement>(null);
  //   const [duration, setDuration] = useState(60);
  //   const [started, setStarted] = useState(false);
  //   const [ended, setEnded] = useState(false);
  const [index, setIndex] = useState(0);
  const [correctIndex, setCorrectIndex] = useState(0);
  const [errorIndex, setErrorIndex] = useState(0);
  const [quote, setQuote] = useState<typeof quotesArray[0]>({ quote: "" });
  const [input, setInput] = useState("");
  const [cpm, setCpm] = useState(0);
  const [wpm, setWpm] = useState(0);
  const [accuracy, setAccuracy] = useState(0);
  const [isError, setIsError] = useState(false);
  const [lastScore, setLastScore] = useState("0");
  const getData = () => ({
    points: 0,
    gameInfo: {
      accuracy: accuracy,
      characterPreMinute: cpm,
      errors: errorIndex,
      wordPreMinute: wpm,
      timer: 0,
    },
  });
  const {
    handleStart,
    gameState,
    time,
    handleEnd: handleEndGame,
  } = useGame<SpeedGame>("speedGame", getData);
  const sendData = useSendGameData("speedGame");
  useEffect(() => {
    if (gameState === "end")
      sendData(10, {
        accuracy: accuracy,
        characterPreMinute: cpm,
        errors: errorIndex,
        wordPreMinute: wpm,
        timer: 0,
      })
  }, [gameState])
  const getQuote = () => {
    const newQuote = random(quotesArray);
    setQuote(newQuote);
    setInput(newQuote.quote);
  };

  useEffect(() => {
    getQuote();
  }, []);

  const handleEnd = () => {
    handleEndGame();
    clearInterval(interval);
  };

  //   const setTimer = () => {
  //     const now = Date.now();
  //     const seconds = now + duration * 1000;
  //     interval = setInterval(() => {
  //       const secondLeft = Math.round((seconds - Date.now()) / 1000);
  //       setDuration(secondLeft);
  //       if (secondLeft === 0) {
  //         handleEnd();
  //       }
  //     }, 1000);
  //   };

  const startGame = () => {
    handleStart();
    // setStarted(true);
    // setEnded(false);
    setInput(quote.quote);
    inputRef?.current?.focus();
    // setTimer();
  };

  const handleKeyDown = (e: any) => {
    e.preventDefault();
    const { key } = e;
    const quoteText = quote.quote;

    if (key === quoteText.charAt(index)) {
      setIndex(index + 1);
      const currenChar = quoteText.substring(
        index + 1,
        index + quoteText.length
      );
      setInput(currenChar);
      setCorrectIndex(correctIndex + 1);
      setIsError(false);
      if (outputRef && outputRef.current) outputRef.current.innerHTML += key;
    } else {
      if (allowedKeys.includes(key)) {
        setErrorIndex(errorIndex + 1);
        setIsError(true);
        if (outputRef && outputRef.current)
          outputRef.current.innerHTML += `<span class="text-danger">${key}</span>`;
      }
    }

    const timeRemains = Number(((60 - time) / 60).toFixed(2));
    const _accuracy = Math.floor(((index - errorIndex) / index) * 100);
    const _wpm = Math.round(correctIndex / 5 / timeRemains);

    if (index > 5) {
      setAccuracy(_accuracy);
      setCpm(correctIndex);
      setWpm(_wpm);
    }

    if (index + 1 === quoteText.length || errorIndex > 50) {
      handleEnd();
    }
  };

  useEffect(() => {
    if (gameState === "end") localStorage.setItem("wpm", wpm.toString());
  }, [gameState, wpm]);
  useEffect(() => {
    const stroedScore = localStorage.getItem("wpm");
    if (stroedScore) setLastScore(stroedScore);
  }, []);

  return (
    <div className="App">
      <div className="container-fluid pt-4">
        <div className="row">
          <div className="col-sm-6 col-md-2 order-md-2 px-5">
            <ul className="list-unstyled text-center small">
              <ItemList name="Время" data={time} />
              <ItemList name="Ошибки" data={errorIndex} />
              <ItemList name="Аккуратность" data={accuracy} symbol="%" />
            </ul>
          </div>
          {/* Left */}
          <div className="col-sm-6 col-md-2 order-md-0 px-5">
            <ul className="list-unstyled text-center small">
              <ItemList
                name="WPM"
                data={wpm}
                style={{
                  color: wpm && "white",
                  backgroundColor:
                    wpm > 0 && wpm < 20
                      ? "#eb4841"
                      : wpm >= 20 && wpm < 40
                      ? "#f48847"
                      : wpm >= 40 && wpm < 60
                      ? "#ffc84a"
                      : wpm >= 60 && wpm < 80
                      ? "#a6c34c"
                      : wpm >= 80
                      ? "#4ec04e"
                      : "",
                }}
              />
              <ItemList name="CPM" data={cpm} />
              <ItemList name="Последний счёт" data={Number(lastScore)} />
            </ul>
          </div>
          {/* Body */}
          <div className="col-sm-12 col-md-8 order-md-1">
            <div className="container">
              <div className="text-center mt-4 header">
                <div className="alert alert-danger" role="alert">
                  Просто начинайте писать. Не используйте <b>backspace</b> чтобы
                  исправить ошибки. Ошибки будут помечены <u>красным</u> и
                  показаны в окне ввода.
                </div>

                <div className="control my-5">
                  <GameButton state={gameState} handleStart={startGame} />
                  {/*                     
                  {ended ? (
                    <button
                      className="btn btn-outline-danger btn-circle"
                      onClick={() => {
                        window.location.reload();
                        // getQuote();
                        // setEnded(false);
                        // if (outputRef && outputRef.current)
                        //   outputRef.current.innerHTML = "";
                        // setDuration(60);
                        // getQuote();

                        // // handleStart()
                      }}
                    >
                      Reload
                    </button>
                  ) : started ? (
                    <button
                      className="btn btn-circle btn-outline-success"
                      disabled
                    >
                      Hurry
                    </button>
                  ) : (
                    <button
                      className="btn btn-circle btn-outline-success"
                      onClick={handleStart}
                    >
                      GO!
                    </button>
                  )} */}
                  <span className="btn-circle-animation" />
                </div>
              </div>

              {gameState === "end" ? (
                <div className="bg-dark text-light p-4 mt-5 lead rounded">
                  <span>"{quote.quote}"</span>
                </div>
              ) : gameState === "start" ? (
                <div
                  className={`text-light mono quotes${
                    gameState === "start" ? " active" : ""
                  }${isError ? " is-error" : ""}`}
                  tabIndex={0}
                  onKeyDown={handleKeyDown}
                  ref={inputRef}
                >
                  {input}
                </div>
              ) : (
                <div
                  className="mono quotes text-muted"
                  tabIndex={-1}
                  ref={inputRef}
                >
                  {input}
                </div>
              )}

              <div
                className="p-4 mt-4 bg-dark text-light rounded lead"
                ref={outputRef}
              />

              <ul>
                <li>
                  WPM - это показатель, используемый для вычисления уровня
                  владения клавиатурой путем подсчета количества слов, которые
                  можно набрать без ошибок за одну минуту.
                </li>
                <li>CPM вычисляет, сколько символов вводится в минуту.</li>
              </ul>
              <hr className="my-4" />
              <div className="mb-5">
                <div className="d-flex text-white meter-gauge">
                  <span className="col" style={{ background: "#eb4841" }}>
                    0 - 20 Медленно
                  </span>
                  <span className="col" style={{ background: "#f48847" }}>
                    20 - 40 Средне
                  </span>
                  <span className="col" style={{ background: "#ffc84a" }}>
                    40 - 60 Быстро
                  </span>
                  <span className="col" style={{ background: "#a6c34c" }}>
                    60 - 80 Профессионально
                  </span>
                  <span className="col" style={{ background: "#4ec04e" }}>
                    80 - 100+ Лучший результат
                  </span>
                </div>
              </div>
            </div>
          </div>

          {/* <div className="col-sm-6 col-md-2 order-md-2 px-5">
						<ul className="list-unstyled text-center small">
							<ItemList name="Timers" data={duration} />
							<ItemList name="Errors" data={errorIndex} />
							<ItemList name="Acuracy" data={accuracy} symble="%" />
						</ul>
					</div> */}
        </div>
      </div>
    </div>
  );
};
