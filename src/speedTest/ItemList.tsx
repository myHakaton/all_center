import React from 'react'

type ItemProps = {
    name: string, 
    data: number, 
    symbol?: string
    style?: any
}

const ItemList: React.FC<ItemProps> = ({ name, data, symbol, ...rest }) => {
    return (
        <li className="list-item my-1 py-4 border rounded" {...rest}>
            {name}
            <span className="d-block display-4">
                {data}
                {symbol && data > 0 ? <small>{symbol}</small> : ''}
            </span>
        </li>
    )
}

export default ItemList
