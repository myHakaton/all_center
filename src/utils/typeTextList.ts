export const TypeTextList = [{
    type: "snils",
    text: 'За обработку СНИЛС'
}, {
    type: "driversLicense",
    text: 'За обработку В/У'
}, {
    type: "passportRF",
    text: 'За обработку паспорта РФ'
}, {
    type: "zagranpasport",
    text: 'За обработку загнар. паспорта'
}, {
    type: 'speedGame',
    text: 'За прохождение игры Скорость печати'
}, {
    type: 'cookieClickerGame',
    text: 'За прохождение игры КукиКликер'
}, {
    type: 'tabGame',
    text: 'За прохождение игры Весёлый Tab'
}, {
    type: 'searchGame',
    text: 'За прохождение игры Внимание док'
}];