export const countAccuracy = (index: number, errorIndex: number) => {
  const accuracy = Math.floor(((index - errorIndex) / index) * 100);
  if (accuracy < 0 || isNaN(accuracy)) return 0;
  return accuracy;
};
