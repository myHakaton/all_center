export const TaskList = [{
    title: 'Паспорту',
    description: 'Требуется обработать 20 паспортов за одну смену',
    img: 'https://www.kino-teatr.ru/movie/kadr/97646/261688.jpg'
}, {
    title: 'Следующий уровень',
    description: 'Дает неуязвимость к документам на 3 часа',
    img: 'https://marinanappa.com/wp-content/uploads/2016/02/kak-vyjti-na-drugoj-uroven.jpg'
}, {
    title: 'Самый быстрый на Диком Западе',
    description: 'Повысить скорость обработки документов',
    img: 'https://cs9.pikabu.ru/post_img/big/2018/04/08/5/1523168411139291540.jpg'
}, {
    title: 'Мистер Безошибочный',
    description: 'Обработай 20 документов без единой ошибки',
    img: 'https://icdn.lenta.ru/images/2018/06/20/15/20180620152956797/detail_e65ff8536a74dfc22b9160a2cdcbf3cb.jpg'
}];