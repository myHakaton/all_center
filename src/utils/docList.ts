import { DocList } from "../types";

export const docList: DocList[] = [
  {
    type: "snils",
    img: "https://vesti-lipetsk.ru/images/news/2020/04/26/snils_-obrazec.jpg",
    complexity: 1,
    values: [
      {
        label: "СНИЛС номер",
        name: "number",
      },
      {
        label: "Фамилия",
        name: "surname",
      },
      {
        label: "Имя",
        name: "name",
      },
      {
        label: "Отчество",
        name: "middleName",
      },
      {
        label: "Дата рождения",
        name: "dateOfBirth",
      },
      {
        label: "Место рождения",
        name: "placeOfBirth",
      },
      {
        label: "Дата регистрации",
        name: "registrationDate",
      },
    ],
  },
  {
    type: "zagranpasport",
    img: "https://zagranportal.ru/wp-content/uploads/2019/02/zagranpasport-starogo-obrazca.jpg",
    complexity: 4,
    values: [
      {
        label: "Серия",
        name: "series",
      },
      {
        label: "Номер",
        name: "number",
      },
      {
        label: "Фамилия",
        name: "surname",
      },
      {
        label: "Имя",
        name: "name",
      },
      {
        label: "Отчество",
        name: "middleName",
      },
      {
        label: "Гражданство",
        name: "citizenship",
      },
      {
        label: "Дата рождения",
        name: "dateOfBirth",
      },
      {
        label: "Место рождения",
        name: "placeOfBirth",
      },
      {
        label: "Дата выдачи",
        name: "dateOfIssue",
      },
      {
        label: "Дата окончания срока",
        name: "expirationDate",
      },
      {
        label: "Орган выдавший документ",
        name: "issuingAuthority",
      },
    ],
  },
  {
    type: "passportRF",
    img: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Pasport_RF.jpg/274px-Pasport_RF.jpg",
    complexity: 3,
    values: [
      {
        label: "Серия",
        name: "series",
      },
      {
        label: "Номер",
        name: "number",
      },
      {
        label: "Фамилия",
        name: "surname",
      },
      {
        label: "Имя",
        name: "name",
      },
      {
        label: "Отчество",
        name: "middleName",
      },
      {
        label: "Дата рождения",
        name: "dateOfBirth",
      },
      {
        label: "Место рождения",
        name: "placeOfBirth",
      },
      {
        label: "Орган выдавший документ",
        name: "issuingAuthority",
      },
      {
        label: "Дата выдачи",
        name: "dateOfIssue",
      },
      {
        label: "Код подразделения",
        name: "codeIssue",
      },
    ],
  },
  {
    type: "driversLicense",
    img: "https://www.avtogai.ru/pictures/obr1.jpg",
    complexity: 2,
    values: [
      {
        label: "Серия",
        name: "series",
      },
      {
        label: "Номер",
        name: "number",
      },
      {
        label: "Фамилия",
        name: "surname",
      },
      {
        label: "Имя",
        name: "name",
      },
      {
        label: "Отчество",
        name: "middleName",
      },
      {
        label: "Дата рождения",
        name: "dateOfBirth",
      },
      {
        label: "Место рождения",
        name: "placeOfBirth",
      },
      {
        label: "Место жительства",
        name: "placeOfResidence",
      },
      {
        label: "Дата выдачи",
        name: "dateOfIssue",
      },
      {
        label: "Действителен до",
        name: "expirationDate",
      },
      {
        label: "Стаж вождения",
        name: "drivingExperience",
      },
    ],
  },
];
