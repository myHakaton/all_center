import React, { useState, useEffect, useRef, useMemo } from "react";

import CookieImage from "./cookie.svg";

import styles from "./cookie.module.css";
import { GameButton } from "../components";
import { CookieClickerGame, GameState } from "../types";
import { countAccuracy } from "../utils/accuracy";
import { useGame, useSendGameData } from "../hooks";

const allowedKeys = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя;,.";

type ItemProps = {
  name: string;
  data: any;
  style?: any;
};

const ItemList: React.FC<ItemProps> = ({ name, data, ...rest }) => {
  return (
    <li className="list-item my-1 py-4 border rounded" {...rest}>
      {name}
      <span className="d-block display-4">
        {data}
        {/* {symbol && data > 0 ? <small>{symbol}</small> : ''} */}
      </span>
    </li>
  );
};

let interval: any;

const random = (array: any) => array[Math.floor(Math.random() * array.length)];
export function CookieClicker() {
  const [clicks, setClicks] = useState(0);
  const [activeKey, setActiveKey] = useState("а");
  const [pressedKey, setPressedKey] = useState();
  const [fails, setFails] = useState(0);

  // const [gameState, setGameState] = useState<GameState>("idle");
  const getData = (clicks: any) => {
    return {
      points: clicks,
      gameInfo: { errors: fails, timer: 0 },
    };
  }
  const { handleStart, gameState, time } = useGame<CookieClickerGame>(
    "cookieClickerGame",
    () => getData(clicks)
  );

  const sendData = useSendGameData("cookieClickerGame");
  useEffect(() => {
    if (gameState === "end")
      sendData(clicks, { errors: fails, timer: 0 })
  }, [gameState])

  const handleKeyDown = (e: any) => {
    const { key } = e;
    setClicks((clicks) => {
      const isCorrect = key === activeKey;
      if (isCorrect) {
        animateCookie(clicks, "up");
        setActiveKey(random(allowedKeys));
        e.preventDefault();
        return clicks + 1;
      } else if (allowedKeys.includes(key)) {
        animateCookie(clicks, "down");
        setPressedKey(key);
        setFails(fails + 1);
        return clicks - 2 < 0 ? 0 : clicks - 2;
      }
      return clicks;
    });
  };

  useEffect(() => {
    if (gameState === "start") {
      document.addEventListener("keydown", handleKeyDown);
      return () => document.removeEventListener("keydown", handleKeyDown);
    }
  }, [activeKey, fails, gameState]);

  useEffect(() => {
    if (pressedKey) {
      const timer = setTimeout(() => setPressedKey(undefined), 700);
      return () => clearTimeout(timer);
    }
  }, [pressedKey]);
  return (
    <div className="container-fluid pt-4">
      <div className="row">
        <div className="col-sm-6 col-md-2 order-md-2 px-5">
          <ul className="list-unstyled text-center small">
            <ItemList name="Время" data={time} />
            <ItemList name="Ошибки" data={fails} />
            <ItemList name="Аккуратность" data={countAccuracy(clicks, fails)} />
          </ul>
        </div>
        {/* Left */}
        <div className="col-sm-6 col-md-2 order-md-0 px-5">
          <ul className="list-unstyled text-center small">
            <ItemList name="Очки" data={clicks} />
          </ul>
        </div>
        {/* Body */}
        <div className="col-sm-12 col-md-8 order-md-1">
          <div className="container">
            <div className="text-center mt-4 header">
              <div className="alert alert-danger" role="alert">
                Нажимайте на буквы, чтобы печенька росла. Не используйте{" "}
                <b>мышь</b> чтобы пытаться её вырастить. За каждую неверно
                нажатую букву будет добавлена одна ошибка.
              </div>

              <div className="control my-5">
                <span className="btn-circle-animation" />
              </div>
              <div className="control my-5">
                <GameButton state={gameState} handleStart={handleStart} />
                <span className="btn-circle-animation" />
              </div>
            </div>
            {gameState === "start" && (
              <span className="d-block display-4">
                Нажмите клавишу: {activeKey}
              </span>
            )}
            <br />
            <br />
            <br />
            {/* Клавиша: <span className="clicks">{activeKey}</span> */}
            {/* {activeKey && <ItemList name="Нажмите" data={activeKey} />} */}
            <div className={styles.cookieContainer}>
              <img
                id="test_cookie"
                src={CookieImage}
                onClick={(e) => setFails(clicks + 1)}
                className={styles.cookie}
                alt="Cookie"
              />
            </div>
            <div
              className="col-sm-6 col-md-2 order-md-2 px-5"
              style={{ float: "right" }}
            >
              {pressedKey && (
                <ItemList
                  name="Клавиша"
                  data={pressedKey}
                  style={{
                    backgroundColor: pressedKey === activeKey ? "green" : "red",
                  }}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function animateCookie(clicks: number, action: string) {
  const cookie = document.querySelector("." + styles.cookie) as any;
  const container = document.querySelector("." + styles.cookieContainer) as any;
  const update = (clicks: number, size: number = 100) =>
    clicks * 10 + size + "px";
  // action === 'up' ? clicks + size + "px" : clicks - size + "px"
  if (cookie) {
    if (container && container.style) {
      container.style.width = update(clicks);
      container.style.height = update(clicks);
    }
    cookie.style.marginTop = update(0, -10);
    setTimeout(() => {
      cookie.style.width = update(clicks);
      cookie.style.height = update(clicks);
      cookie.style.marginTop = "0";
    }, 20);
  }
}
