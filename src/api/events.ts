import axios from "axios";
import { GameResult } from "../types";

export const sendGameData = async (
  user: any,
  gameName: string,
  points: number,
  gameInfo: GameResult
) => {
  const res = await axios.post("/api/events/create/", {
    userId: user.id,
    type: gameName,
    data: {
      [gameName]: { ...gameInfo },
    },
    points,
  });
  return res.data;
};
