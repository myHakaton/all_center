import {Card, Col, Image, Progress, Row} from "antd";
import React from "react";

interface ProgressDeckCardProps {
    title: string;
    description: string;
    percent: number;
    img?: string;
}

export const ProgressDeckCard: React.FC<ProgressDeckCardProps> = (props) => {
    return (
        <Col span={24}>
            <Card className={'ProgressDeck'}
                  hoverable
                // cover={
                //     <Image alt="example"
                //            src="error"
                //            height={150}
                //            fallback={props.img}
                //     />
                // }
            >
                <Row className={'Text'}>
                    <div className={'ProgressDeckTitle'}>
                        {props.title}
                    </div>
                    <div className={'ProgressDeckNumber'} style={{color: 'green'}}>
                        {props.percent}%
                    </div>
                </Row>
                <Row className={'Description'}>
                    {props.description}
                </Row>
                <Row>
                    <Progress percent={props.percent} showInfo={false} strokeColor={'green'}/>
                </Row>
            </Card>
        </Col>
    )
};