import React, {useEffect, useState} from "react";
import {Col, Descriptions, Divider, Image, Modal, Row, Table} from "antd";
import {PolarArea} from 'react-chartjs-2';
import './index.css'
import {ProgressDeckCard} from "./progressDeckCard";
import {useRecoilValue} from "recoil";
import {UserListState, UserState} from "../layout";
import {CrownOutlined} from "@ant-design/icons";
import axios from "axios";
import moment from "moment";
import {TypeTextList} from "../utils/typeTextList";

export const LkComponent: React.FC = () => {
    const user = useRecoilValue(UserState);
    const userList = useRecoilValue(UserListState);
    const [show, setShow] = useState(false);
    const [history, setHistory] = useState<any>();
    const data = {
        labels: ['Внимательность', 'Качество', 'Скорость', 'Ошибки'],
        datasets: [
            {
                label: '# of Votes',
                data: [36, 66, 79, 22],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 206, 86, 0.5)',
                    'rgba(75, 192, 192, 0.5)'
                ],
                borderWidth: 1
            },
        ],
    };
    useEffect(() => {
        if (!user) return;
        axios.post('/api/events/get/by/user', {
            userId: user.id
        }).then(({data}) => setHistory(data));
    }, [user]);
    return (
        <div>
            <Row>
                <Divider/>
            </Row>
            <Row>
                <Col span={6} style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                    <Image
                        width={200}
                        height={200}
                        src="error"
                        fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                    />
                </Col>
                <Col span={12} style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                    <Descriptions size={'small'} bordered title="Информация о сотруднике" column={2}>
                        <Descriptions.Item label="ФИО">{user && user.name}</Descriptions.Item>
                        <Descriptions.Item label="Уровень">{user && user.rank}</Descriptions.Item>
                        <Descriptions.Item label="Ваша внимательноть" span={24}>
                            {`выше чем у ${'30%'} сотрудников`}
                        </Descriptions.Item>
                        <Descriptions.Item label="Вы ошибаетесь" span={24}>
                            {`меньше чем ${'15%'} сотрудников`}
                        </Descriptions.Item>
                        <Descriptions.Item label="Ваша скорость обработки" span={24}>
                            {`выше чем у ${'45%'} сотрудников`}
                        </Descriptions.Item>
                        <Descriptions.Item label="Очки" span={24}>
                            <div
                                style={{display: "flex", alignItems: "center", cursor: "pointer"}}
                                onClick={() => {
                                    setShow(true);
                                }}
                            >
                                <div style={{fontSize: 24}}>
                                    {user && user.earnedPoints}
                                </div>
                                <CrownOutlined style={{paddingLeft: 5, color: 'orange', fontSize: 32}}/>
                            </div>
                        </Descriptions.Item>
                    </Descriptions>
                </Col>
                <Col span={6} style={{padding: 40, display: "flex", justifyContent: "center", alignItems: "center"}}>
                    <PolarArea data={data}
                               options={{
                                   plugins: {
                                       legend: {
                                           display: false,
                                       },
                                   }
                               }}/>
                </Col>
            </Row>
            <Row>
                <Divider/>
            </Row>
            <Row>
                <Col span={6} style={{padding: 10}}>
                    <Table
                        bordered
                        title={() => ('Золотая лига')}
                        pagination={false}
                        columns={[{
                            title: 'ФИО',
                            dataIndex: 'name'
                        }, {
                            title: 'Очки',
                            dataIndex: 'earnedPoints',
                            render: (value) => (
                                <div style={{display: "flex", alignItems: "center"}}>
                                    <div>{value}</div>
                                    <CrownOutlined style={{color: 'orange', paddingLeft: 5, fontSize: 12}}/>
                                </div>
                            )
                        }]}
                        dataSource={userList}
                    />
                </Col>
                <Col span={6} style={{padding: 10}}>
                    <Table
                        bordered
                        title={() => ('Серебряная лига')}
                        columns={[{
                            title: 'ФИО',
                            dataIndex: 'name'
                        }, {
                            title: 'Очки',
                            dataIndex: 'earnedPoints'
                        }]}
                        dataSource={[]}
                    />
                </Col>
                <Col span={6} style={{padding: 10}}>
                    <Table
                        bordered
                        title={() => ('Бронзовая лига')}
                        columns={[{
                            title: 'ФИО',
                            dataIndex: 'name'
                        }, {
                            title: 'Очки',
                            dataIndex: 'earnedPoints'
                        }]}
                        dataSource={[]}
                    />
                </Col>
                <Col span={6} style={{padding: 10}}>
                    {user && user.data.achieves.map((item: any) => (
                        <ProgressDeckCard title={item.name} description={item.description} percent={item.percent}/>
                    ))}
                </Col>
            </Row>
            <Modal title={'Детализация'} visible={show}
                   okButtonProps={{hidden: true}}
                   onCancel={() => setShow(false)}
                   width={600}
            >
                <Table
                    pagination={false}
                    columns={[{
                        title: 'Время',
                        dataIndex: 'createTime',
                        render: (date) => moment(date).format('DD.MM.YYYY HH:mm:ss')
                    }, {
                        title: 'Тип',
                        dataIndex: 'type',
                        render: (type) => {
                            return TypeTextList.find(item => item.type === type)?.text
                        }
                    }, {
                        title: 'Очки',
                        dataIndex: 'points'
                    }]}
                    dataSource={history}
                />
            </Modal>
        </div>
    )
};