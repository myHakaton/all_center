import {Button, Col, Descriptions, Divider, Form, Image, Input, Progress, Row, Statistic} from "antd";
import {docList} from "../utils/docList";
import React, {useCallback, useEffect, useState} from "react";
import lodash from 'lodash';
import moment from 'moment';
import {v4} from 'uuid';
import {useForm} from "antd/es/form/Form";
import {useRecoilState, useRecoilValue} from "recoil";
import {UserListState, UserState} from "../layout";
import axios from "axios";
import {CrownOutlined, HeartOutlined, HeartTwoTone, WindowsFilled} from "@ant-design/icons";
import {usePvP} from "../hooks";

(window as any).axios = axios

export const VerificationComponent: React.FC = () => {
    const user = useRecoilValue(UserState);
    const [reload, setReload] = useState<string>('')
    const [docId, setDocId] = useState<number>();
    const [startTime, setStartTime] = useState<moment.Moment>();
    const [time, setTime] = useState<moment.Moment>();
    const [form] = useForm();
    const [userList, setUserList] = useRecoilState(UserListState);
    const [maxPointsResult, setMaxPointsResult] = useState<number>(0);
    const [minPointsResult, setMinPointsResult] = useState<number>(0);
    const [twoUserName, setTwoUserName] = useState('');
    const [isLeader, setIsLeader] = useState(false);
    // const [percent, setPercent] = useState(10);
    // const [earnedPoints, setEarnedPoints] = useState();
    // useEffect(() => {
    //     if (!time) return;
    //     if (percent === 100) {
    //         setEarnedPoints(earnedPoints + 50);
    //         setPercent(0);
    //     } else setPercent(percent + 10);
    // }, [time]);
    useEffect(() => {
        const timer = setInterval(() => {
            setTime(moment());
        }, 1000);
        return () => {
            clearInterval(timer);
        }
    }, []);
    useEffect(() => {
        setStartTime(moment());
        setDocId(lodash.random(0, 3));
    }, [reload]);
    useEffect(() => {
        if (docId === undefined) return;
        axios.post('/api/tourney/current/with/user', {
            type: docList[docId].type,
            userId: user.id
        }).then(({data}) => {
            const list = data.result.results;
            if (list.length === 0 || list.length < 2) return;
            const resultIndex = list.findIndex((item: any) => item.username === user.name);
            if (resultIndex === -1) return;
            if (resultIndex !== 0) {
                setIsLeader(false);
                setTwoUserName(list[0].username);
                setMaxPointsResult(list[0].earnedPoints);
                setMinPointsResult(list[resultIndex].earnedPoints);
            } else {
                setIsLeader(true);
                setTwoUserName(list[1].username);
                setMaxPointsResult(list[resultIndex].earnedPoints);
                setMinPointsResult(list[1].earnedPoints);
            }
        });
    }, [docId, user]);
    return (
        <div>
            <Row>
                <Divider/>
            </Row>
            <Row style={{padding: 15}}>
                <Col span={8} style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                    <Button type={"dashed"} size={"large"}>
                        Отойти на перерыв
                    </Button>
                </Col>
                <Col span={8}>
                    <Row justify={"center"}>
                        <Col span={12}>
                            <Statistic
                                title="Ваши очки"
                                value={user && user.earnedPoints}
                                prefix={
                                    <CrownOutlined style={{paddingLeft: 5, color: 'orange', fontSize: 32}}/>
                                }/>
                        </Col>
                        <Col span={12}>
                            <Statistic
                                title="Время обработки"
                                value={
                                    time &&
                                    startTime &&
                                    moment(time.diff(startTime, 'second'), 'X').utc().format('HH:mm:ss')
                                }
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Progress percent={64} showInfo={false}/>
                    </Row>
                </Col>
                <Col span={8} style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                    {maxPointsResult !== 0 && minPointsResult !== 0 &&
                    <Row justify={"center"}>
                        <Col span={12}>
                            <Descriptions size={'small'} column={1}>
                                <Descriptions.Item label={
                                    <div style={{display: "flex"}}>
                                        <div style={{paddingRight: 5}}>
                                            <HeartTwoTone style={{fontSize: 16}} twoToneColor="red"/>
                                            <HeartTwoTone style={{fontSize: 16}} twoToneColor="red"/>
                                            <HeartOutlined style={{fontSize: 11}}/>
                                        </div>
                                        <div>{isLeader ? user.name : twoUserName}</div>
                                    </div>
                                }>
                                    <div style={{display: "flex", alignItems: "center"}}>
                                        <div>{maxPointsResult}</div>
                                        <CrownOutlined style={{color: 'orange', paddingLeft: 5, fontSize: 12}}/>
                                    </div>
                                </Descriptions.Item>
                                <Descriptions.Item label={
                                    <div style={{display: "flex"}}>
                                        <div style={{paddingRight: 5}}>
                                            <HeartTwoTone style={{fontSize: 16}} twoToneColor="red"/>
                                            <HeartOutlined style={{fontSize: 11}}/>
                                            <HeartOutlined style={{fontSize: 11}}/>
                                        </div>
                                        <div>{isLeader ? twoUserName : user.name}</div>
                                    </div>
                                }>
                                    <div style={{display: "flex", alignItems: "center"}}>
                                        <div>{minPointsResult}</div>
                                        <CrownOutlined style={{color: 'orange', paddingLeft: 5, fontSize: 12}}/>
                                    </div>
                                </Descriptions.Item>
                            </Descriptions>
                        </Col>
                    </Row>
                    }
                </Col>
            </Row>
            <Row>
                <Divider/>
            </Row>
            <Row style={{flexGrow: 1, background: '#f7f7f7'}}>
                <Col span={12}
                     style={{
                         padding: 30,
                         display: "flex",
                         flexDirection: "column",
                         justifyContent: "center",
                         alignItems: 'center'
                     }}>
                    <Form.Item>
                        <Button type={"primary"} danger size={"large"}>
                            Невозможно разобрать
                        </Button>
                    </Form.Item>
                    <Form.Item>
                        <Image
                            src="error"
                            fallback={docId !== undefined ? docList[docId].img : ''}
                        />
                    </Form.Item>
                </Col>
                <Col span={12} style={{padding: 30}}>
                    <Form layout={"vertical"}
                          form={form}
                          onFinish={async (values) => {
                              if (docId === undefined || !time || !startTime || !user) return;
                              const type = docList[docId].type;
                              const complexity = docList[docId].complexity;
                              const second = time.diff(startTime, 'second');
                              let symbols: number = 0;
                              Object.entries(values).forEach(([key, value]) => {
                                  const string = value as string;
                                  symbols += string.length;
                              });
                              await axios.post('/api/events/create/', {
                                  userId: user.id,
                                  type,
                                  points: 25 * complexity,
                                  data: {
                                      documentAnalized: {
                                          charCounts: symbols,
                                          difficult: complexity,
                                          hasError: lodash.random(0, 1),
                                          isFinishedInTime: true,
                                          timer: second
                                      }
                                  }
                              });
                              setReload(v4());
                              form.resetFields();
                              setUserList(userList);
                          }}
                    >
                        {docId !== undefined && docList[docId].values.map((item, index) => (
                            <Form.Item key={index} {...item} rules={[{required: true}]}>
                                <Input/>
                            </Form.Item>
                        ))}
                        <Form.Item>
                            <Button type={"primary"} size={"large"}
                                    onClick={() => {
                                        form.submit();
                                    }}>
                                Завершить обработку
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </div>
    )
}