export type GameState = "start" | "end" | "idle";

export type DocList = {
  type: "snils" | "zagranpasport" | "passportRF" | "driversLicense";
  img: string;
  complexity: number;
  values: {
    label: string;
    name: string;
  }[];
};

export interface Result {
  earnedPoints: number;
  winnerUserId: string;
}

export interface User {
  health: number;
  lastResult: number;
  userId: string;
}

export interface Game {
  createTime: Date;
  id: string;
  isFinished: boolean;
  new: boolean;
  result: Result;
  updateTime: Date;
  user1: User;
  user2: User;
}

export interface UserState {
  data?: Data;
  points?: number;
  type?: string;
  userId?: string;
}

export interface Data {
  achivmentEarned?: AchivmentEarned;
  cookieClickerGame?: CookieClickerGame;
  documentAnalized?: DocumentAnalized;
  searchGame?: SearchGame;
  speedGame?: SpeedGame;
  tabGame?: TabGame;
  tourneyWin?: TourneyWin;
}

export type GameName =
  | "cookieClickerGame"
  | "documentAnalized"
  | "searchGame"
  | "speedGame"
  | "tabGame";

export interface AchivmentEarned {
  achivement?: Achivement;
}

export interface Achivement {
  createTime?: Date;
  description?: string;
  duration?: string;
  eventType?: string;
  id?: string;
  name?: string;
  points?: number;
  updateTime?: Date;
}

export interface CookieClickerGame {
  errors?: number;
  score?: number;
  timer?: number;
}

export interface DocumentAnalized {
  charCounts?: number;
  difficult?: number;
  hasError?: boolean;
  isFinishedInTime?: boolean;
  timer?: number;
}

export interface SearchGame {
  accuracy?: number;
  errors?: number;
  timer?: number;
}

export interface SpeedGame {
  accuracy?: number;
  characterPreMinute?: number;
  errors?: number;
  timer?: number;
  wordPreMinute?: number;
}

export interface TabGame {
  accuracy?: number;
  timer?: number;
}

export interface TourneyWin {
  createTime?: Date;
  endTime?: Date;
  id?: string;
  result?: TourneyWinResult;
  startTime?: Date;
  type?: string;
  updateTime?: Date;
}

export interface TourneyWinResult {
  results?: ResultElement[];
}

export interface ResultElement {
  earnedPoints?: number;
  place?: number;
  username?: string;
}

export type GameResult =
  | CookieClickerGame
  | SearchGame
  | SpeedGame
  | TabGame
  | TourneyWin;
