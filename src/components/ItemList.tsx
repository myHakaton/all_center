type ItemProps = {
  name: string;
  data: any;
  style?: any;
};

export const ItemList: React.FC<ItemProps> = ({ name, data, ...rest }) => {
  return (
    <li className="list-item my-1 py-4 border rounded" {...rest}>
      {name}
      <span className="d-block display-4">
        {data}
        {/* {symbol && data > 0 ? <small>{symbol}</small> : ''} */}
      </span>
    </li>
  );
};
