import { useHistory } from "react-router";
import { GameState } from "../types";

type ButtonProps = {
  state: GameState;
  handleStart: any;
  style?: any;
};

export const GameButton: React.FC<ButtonProps> = ({ state, handleStart }) => {
  const history = useHistory();
  if (state === "end")
    return (
      <button
        className="btn btn-outline-danger btn-circle"
        onClick={() => {
          history.push("/");
        }}
      >
        Выйти
      </button>
    );
  else if (state === "start")
    return (
      <button className="btn btn-circle btn-outline-success" disabled>
        Играем!
      </button>
    );
  else
    return (
      <button
        className="btn btn-circle btn-outline-success"
        onClick={handleStart}
      >
        Старт
      </button>
    );
};
