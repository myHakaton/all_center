import { useEffect, useRef, useState } from "react";
import { Game, GameName, GameResult, GameState } from "../types";
import axios from "axios";
import { useRecoilState, useRecoilValue } from "recoil";
import * as api from "../api/events";
import { UserState } from "../layout";

export const useSendGameData = (gameName: string) => {
  const user = useRecoilValue(UserState);

  return async (points: number, gameInfo: GameResult) =>
    await api.sendGameData(user, gameName, points, gameInfo);
};

export const useTimer = (timeout: number, handleStop: any) => {
  const [time, setTime] = useState(timeout);
  const interval = useRef<any>(null);
  const stopTimer = () => {
    handleStop();
    clearInterval(interval.current);
  };

  const setTimer = () => {
    const now = Date.now();
    const seconds = now + time * 1000;
    interval.current = setInterval(() => {
      const secondLeft = Math.round((seconds - Date.now()) / 1000);
      setTime(secondLeft);
      if (secondLeft === 0) {
        handleStop();
        clearInterval(interval.current);
      }
    }, 1000);
  };
  return { time, setTimer, stopTimer };
};

export function useGame<T extends GameResult>(
  gameName: GameName,
  data: () => { points: number; gameInfo: T }
) {
  const [gameState, setGameState] = useState<GameState>("idle");
  const sendData = useSendGameData(gameName);
  const { time, setTimer, stopTimer } = useTimer(60, async () => {
    setGameState("end");
    const { points, gameInfo } = data();
    // await sendData(points, gameInfo);
  });

  const handleStart = () => {
    setTimer();
    setGameState("start");
  };

  const handleEnd = async () => {
    stopTimer();
  };

  return { handleStart, handleEnd, gameState, time };
}

export function useInterval(callback: () => void, delay: number | null) {
  const savedCallback = useRef(callback);

  // Remember the latest callback if it changes.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    // Don't schedule if no delay is specified.
    if (delay === null) {
      return;
    }

    const id = setInterval(() => savedCallback.current(), delay);

    return () => clearInterval(id);
  }, [delay]);
}
export const usePvP = (user: any) => {
  const [pvpState, setPvpState] = useState<GameState>("idle");
  const [game, setGame] = useState<Game>({} as Game);

  const startCompetition = async () => {
    const res = await axios.post("/api/competition/start", {
      userId: user.id,
    });
    setPvpState("start");
    setGame(res.data());
  };

  const fixResult = async () => {
    const res = await axios.post("/api/competition/fix", { userId: user.id });
    setGame(res.data());
  };
  useInterval(
    async () => {
      const res = await axios.post("/api/competition/get", {
        competitionId: game.id,
      });
      setGame(res.data());
    },
    pvpState === "start" && game.id ? 10000 : null
  );

  return { pvpState, game, startCompetition, fixResult };
};
