import React, { Suspense } from 'react';
import './App.css';
import {LayoutComponent} from "./layout";
import {SelectedUserComponent} from "./selectedUser";


export const App = () => {
        const userId = localStorage.getItem('userId');
        return (
            <div className="App" style={{height: '100%'}}>
                {userId &&
                <Suspense fallback={'Загрузка...'}>
                    <LayoutComponent/>
                </Suspense>
                }
                {!userId &&
                <SelectedUserComponent/>
                }
            </div>
        )
    }
;
